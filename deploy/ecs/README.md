# ECS Deploy Image

Used for deploying images to ECS via GitLab CI

Build:

```
docker build . -t 5stones/ecs-deploy
docker push 5stones/ecs-deploy
```
