# Common Deploy Image

Used for deploying images via Terraform & GitLab CI

Build:

```
docker build . -t 5stones/common-deploy
docker login --username yourname
docker push 5stones/common-deploy:latest
```

Tag (after building):

```
docker tag 5stones/common-deploy:latest 5stones/common-deploy:4.x.0
docker push 5stones/common-deploy:4.x.0
```
